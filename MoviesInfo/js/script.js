
const src='https://api.themoviedb.org/3/search/movie';
const src2='https://api.themoviedb.org/3/movie';
const srcperson='https://api.themoviedb.org/3/search/person'
const srcmoviebyperson='https://api.themoviedb.org/3/discover/movie';
const key='81c4c46722b411525a783b45815ba17d';
const trending='https://api.themoviedb.org/3/movie/top_rated'

var page=1;
var totalPage=0;
var strSearchTemp="a";

function loading(){
    $('#main').empty();
    $('#headTitle').empty();
    $('#headTitle').append(`
        <div class="spinner-grow" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    `);
}

async function prepare(e,pageID)
{
    e.preventDefault();
    $('#headTitle').empty();
    $('#main').empty();
    $('#navaBot').empty();
    $('#cmt').empty();
    $('#actress').empty();
    $('#info').empty();
    const reqStr=`${trending}?api_key=${key}&page=${pageID}`;
    loading();

    const response=await fetch(reqStr);
    const rs=await response.json();
    console.log(rs);

    page=pageID;
    totalPage=rs.total_pages;

    $('#headTitle').empty();
    $('#headTitle').append(`
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"><h5 style="text-align: center; font-size: 40; font-weight: bold;">Top rate</h5></div>
        </div>
    `);

    fillMovies(rs.results);
    
    if(page==1){
        $('#navaBot').empty();
        $('#navaBot').append(`
            <nav aria-label="Page navigation example" style="margin-top: 30;">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <a class="page-link" onclick="prepare(event,1)" tabindex="-1" aria-disabled="true">Previous</a>
                    </li>
                    <li class="page-item active"><a class="page-link" onclick="prepare(event,1)">1</a></li>
                    <li class="page-item"><a class="page-link" onclick="prepare(event,2)">2</a></li>
                    <li class="page-item"><a class="page-link" onclick="prepare(event,3)">3</a></li>
                    <li class="page-item"><a class="page-link" onclick="prepare(event,4)">4</a></li>
                    <li class="page-item"><a class="page-link" onclick="prepare(event,5)">5</a></li>
                    <li class="page-item">
                        <a class="page-link" onclick="prepare(event,2)">Next</a>
                    </li>
                </ul>
            </nav>
        `);
    }
    else{
        $('#navaBot').empty();
        $('#navaBot').append(`
            <nav aria-label="Page navigation example" style="margin-top: 30;">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <a class="page-link" onclick="prepare(event,${page-1})" tabindex="-1" aria-disabled="true">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" onclick="prepare(event,${page-1})">${page-1}</a></li>
                    <li class="page-item active"><a class="page-link" onclick="prepare(event,${page})">${page}</a></li>
                    <li class="page-item"><a class="page-link" onclick="prepare(event,${page+1})">${page+1}</a></li>
                    <li class="page-item"><a class="page-link" onclick="prepare(event,${page+2})">${page+2}</a></li>
                    <li class="page-item"><a class="page-link" onclick="prepare(event,${page+3})">${page+3}</a></li>
                    <li class="page-item">
                        <a class="page-link" onclick="prepare(event,2)">Next</a>
                    </li>
                </ul>
            </nav>
        `);
    }
}

async function evtSubmit(e,pageID)
{
    e.preventDefault();
    $('#headTitle').empty();
    $('#main').empty();
    $('#navaBot').empty();
    $('#cmt').empty();
    $('#actress').empty();
    $('#info').empty();
    const strSearch=$('form input').val();
    const reqStr=`${src}?api_key=${key}&query=${strSearch}&page=${pageID}`;
    loading();

    const response=await fetch(reqStr);
    const rs=await response.json();

    page=pageID;
    totalPage=rs.total_pages;

    console.log(rs.results);

    $('#headTitle').empty();
    $('#headTitle').append(`
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"><h5 style="text-align: center; font-size: 40; font-weight: bold;">Result of "${strSearch}"</h5></div>
        </div>
    `);

    fillMovies(rs.results);

    if(page==1){
        $('#navaBot').empty();
        $('#navaBot').append(`
            <nav aria-label="Page navigation example" style="margin-top: 30;">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <a class="page-link" onclick="evtSubmit(event,1)" tabindex="-1" aria-disabled="true">Previous</a>
                    </li>
                    <li class="page-item active"><a class="page-link" onclick="evtSubmit(event,1)">1</a></li>
                    <li class="page-item"><a class="page-link" onclick="evtSubmit(event,2)">2</a></li>
                    <li class="page-item"><a class="page-link" onclick="evtSubmit(event,3)">3</a></li>
                    <li class="page-item"><a class="page-link" onclick="evtSubmit(event,4)">4</a></li>
                    <li class="page-item"><a class="page-link" onclick="evtSubmit(event,5)">5</a></li>
                    <li class="page-item">
                        <a class="page-link" onclick="evtSubmit(event,2)">Next</a>
                    </li>
                </ul>
            </nav>
        `);
    }
    else{
        $('#navaBot').empty();
        $('#navaBot').append(`
            <nav aria-label="Page navigation example" style="margin-top: 30;">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <a class="page-link" onclick="evtSubmit(event,${page-1})" tabindex="-1" aria-disabled="true">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" onclick="evtSubmit(event,${page-1})">${page-1}</a></li>
                    <li class="page-item active"><a class="page-link" onclick="evtSubmit(event,${page})">${page}</a></li>
                    <li class="page-item"><a class="page-link" onclick="evtSubmit(event,${page+1})">${page+1}</a></li>
                    <li class="page-item"><a class="page-link" onclick="evtSubmit(event,${page+2})">${page+2}</a></li>
                    <li class="page-item"><a class="page-link" onclick="evtSubmit(event,${page+3})">${page+3}</a></li>
                    <li class="page-item">
                        <a class="page-link" onclick="evtSubmit(event,2)">Next</a>
                    </li>
                </ul>
            </nav>
        `);
    }
}

async function searchByPerson(e,pageID){
    e.preventDefault();
    $('#headTitle').empty();
    $('#main').empty();
    $('#navaBot').empty();
    $('#cmt').empty();
    $('#actress').empty();
    $('#info').empty();
    loading();
    const strSearch=$('form input').val();
    reqStrForPerson=`https://api.themoviedb.org/3/search/person?api_key=${key}&query=${strSearch}`;
    const responsePerson=await fetch(reqStrForPerson);
    rsPerson=await responsePerson.json();
    //console.log(rsPerson);

    idPerson=rsPerson.results[0].id;
    //console.log(idPerson)
    

    const reqStr2=`${srcmoviebyperson}?api_key=${key}&with_cast=${idPerson}&page=${pageID}`;
    const response2=await fetch(reqStr2);
    const rs2=await response2.json();
    page=pageID;
    //console.log(rs2.results);
    $('#headTitle').empty();
    $('#headTitle').append(`
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"><h5 style="text-align: center; font-size: 40; font-weight: bold;">Result of "${strSearch}"</h5></div>
        </div>
    `);

    fillMovies(rs2.results);
    
    if(page==1){
        $('#navaBot').empty();
        $('#navaBot').append(`
            <nav aria-label="Page navigation example" style="margin-top: 30;">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <a class="page-link" onclick="searchByPerson(event,1)" tabindex="-1" aria-disabled="true">Previous</a>
                    </li>
                    <li class="page-item active"><a class="page-link" onclick="searchByPerson(event,1)">1</a></li>
                    <li class="page-item"><a class="page-link" onclick="searchByPerson(event,2)">2</a></li>
                    <li class="page-item"><a class="page-link" onclick="searchByPerson(event,3)">3</a></li>
                    <li class="page-item"><a class="page-link" onclick="searchByPerson(event,4)">4</a></li>
                    <li class="page-item"><a class="page-link" onclick="searchByPerson(event,5)">5</a></li>
                    <li class="page-item">
                        <a class="page-link" onclick="searchByPerson(event,2)">Next</a>
                    </li>
                </ul>
            </nav>
        `);
    }
    else{
        $('#navaBot').empty();
        $('#navaBot').append(`
            <nav aria-label="Page navigation example" style="margin-top: 30;">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <a class="page-link" onclick="searchByPerson(event,${page-1})" tabindex="-1" aria-disabled="true">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" onclick="searchByPerson(event,${page-1})">${page-1}</a></li>
                    <li class="page-item active"><a class="page-link" onclick="searchByPerson(event,${page})">${page}</a></li>
                    <li class="page-item"><a class="page-link" onclick="searchByPerson(event,${page+1})">${page+1}</a></li>
                    <li class="page-item"><a class="page-link" onclick="searchByPerson(event,${page+2})">${page+2}</a></li>
                    <li class="page-item"><a class="page-link" onclick="searchByPerson(event,${page+3})">${page+3}</a></li>
                    <li class="page-item">
                        <a class="page-link" onclick="searchByPerson(event,2)">Next</a>
                    </li>
                </ul>
            </nav>
        `);
    }
}

function fillMovies(ms){
    
    $('#main').empty();
    $('#Bot').empty();
    for(const m of ms){
        const id=m.id;
        $('#main').append(`
            <div class="col-md-4">
                <div class="card h-100">
                    <img src="https://image.tmdb.org/t/p/w500${m.poster_path}" class="card-img-top" alt="${m.title}">
                    <div class="card-body">
                        <h5 class="card-title">${m.title}</h5>
                        <p class="card-text">Rate: ${m.vote_average}</p>
                        <p class="card-text">Date ${m.release_date}</p>
                        <button onclick="loadDetail(event,${m.id})" type="submit" class="btn btn-primary">See Detail</button>
                    </div>
                </div>
            </div>  
        `)
    }

}

async function loadDetail(e,id){
    e.preventDefault();
    const reqStr=`${src2}/${id}?api_key=${key}`;
    loading();
    $('#info').empty();
    const response=await fetch(reqStr);
    const rs=await response.json();
    console.log(rs);

    fillMovie(rs);

    const reqStr2=`https://api.themoviedb.org/3/movie/${id}/credits?api_key=${key}`;
    const response2=await fetch(reqStr2);
    const rs2=await response2.json();
    console.log(rs2);

    fillActors(rs2.cast);

    const reqStr3=`https://api.themoviedb.org/3/movie/${rs.id}/reviews?api_key=${key}`;
    const response3=await fetch(reqStr3);
    const rs3=await response3.json();
    console.log(rs3);

    fillReview(rs3.results);
}

function fillMovie(m){
    $('#headTitle').empty();
    $('#main').empty();
    $('#navaBot').empty();
    $('#cmt').empty();
    $('#info').empty();
    $('#headTitle').append(`
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"><h5 style="text-align: center; font-size: 40; font-weight: bold;">${m.title}</h5></div>
        </div>
    `);
    $('#main').append(`
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="card mb-3" style="max-width: 1300px;">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="https://image.tmdb.org/t/p/w500/${m.poster_path}" class="card-img" alt="${m.title}">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <p class="card-text">Over view: ${m.overview}</p>  
                                <p class="card-text">Release date: ${m.release_date}</p> 
                                <p class="card-text">Genre: ${m.genres[0].name}, ${m.genres[1].name}</p>   
                                <p class="card-text">Rate: ${m.vote_average}</p>  
                                <p class="card-text">Length: ${m.runtime} mins</p>          
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `);
}

function fillActors(as)
{
    $('#Bot').empty();
    i=0;
    $('#actress').append(`
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"><h5 style="text-align: center; font-size: 40; font-weight: bold;">Actor</h5></div>
        </div>
    `);
    for(const a of as){
        i++;
        if(i==7)break;
        $('#Bot').append(`
            <div class="col-md-4">
                <div class="card h-100">
                    <img src="https://image.tmdb.org/t/p/w500${a.profile_path}" class="card-img-top" alt="${a.name}">
                    <div class="card-body">
                        <h5 class="card-title">${a.name}</h5>
                        <p class="card-text">${a.character}</p>
                        <button onclick="movieByPerson(event,${a.id},1)" type="submit" class="btn btn-primary">See Detail</button>
                    </div>
                </div>
            </div>  
        `)
    }
}

async function movieByPerson(e,idPerson,pageID)
{
    e.preventDefault;

    const reqPer=`https://api.themoviedb.org/3/person/${idPerson}?api_key=${key}`;
    const responsePer=await fetch(reqPer);
    const Per=await responsePer.json();

    $('#info').empty();
    $('#info').append(`
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="card mb-3" style="max-width: 1000px;">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="https://image.tmdb.org/t/p/w500/${Per.profile_path}" class="card-img" alt="${Per.name}">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <p class="card-text">${Per.name}</p>  
                                <p class="card-text">Birth date: ${Per.birthday}</p>   
                                <p class="card-text">Place of birth: ${Per.place_of_birth}</p>      
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `);

    const reqStr=`${srcmoviebyperson}?api_key=${key}&with_cast=${idPerson}&page=${pageID}`;
    const response=await fetch(reqStr);
    const rs=await response.json();
    page=pageID;
    console.log(Per.name);
    //console.log(rs2.results);
    $('#cmt').empty();
    $('#headTitle').empty();
    $('#actress').empty();
    
    $('#headTitle').append(`
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"><h5 style="text-align: center; font-size: 40; font-weight: bold;">Movie by this actor</h5></div>
        </div>
    `);

    fillMovies(rs.results);
}

function fillReview(rs)
{
    $('#cmt').empty();
    i=0;
    $('#cmt').append(`
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"><h5 style="text-align: center; font-size: 40; font-weight: bold;">Review</h5></div>
        </div>
    `);
    for(const r of rs){
        i++;
        if(i==7)break;
        $('#cmt').append(`
            <div class="card">
                <div class="card-header">
                    ${r.author}
                </div>
                <div class="card-body">
                    <blockquote class="blockquote mb-0">
                        <p style="font-size: 15;">${r.content}</p>
                    </blockquote>
                </div>
            </div>
        `)
    }
}